# FORWARDING A LOCAL NETWORK PORT TO A PORT IN THE POD
kubectl port-forward kubia-manual 8888:8080

## 3.3 Organizing pods with labels

# List pods showing labels
kubectl get po --show-labels

# Modifying labels of existing pods
kubectl label po kubia-manual version=1.2.5

# Force labels modification of existing pods
kubectl label po kubia-manual version=1.2.5 --overwrite

# Add extra column(s) while list any resource,like pods
kubectl get po -L version,env #[,label]

# 3.4 Listing subsets of pods through label selectors
kubectl get po -l version=1.2.5

# To list all pods that include the env label, whatever its value is
kubectl get po -l env

# To list all pods that don’t have the env label
kubectl get po -l '!env'

## 3.6 Annotating pods
# 3.6.2 Adding and modifying annotations
kubectl annotate pod kubia-manual unclemoe.com/moe-annotation="foo bar"

## 3.7 Using namespaces to group resources
# Discovering other namespaces and their pods
kubectl get ns

# List the pods that belong to the kube-system namespace
kubectl get pods --namespace kube-system # Can use -n instead of --namespace.

# CREATING A NAMESPACE WITH KUBECTL CREATE NAMESPACE
kubectl create namespace custom-ns # namespaces (and a few others resources) aren’t allowed to contain dots.

# Managing objects in other namespaces
kubectl create -f kubia-manual.yaml -n custom-ns


