## 5.2.2   Creating the Pod object from the YAML file
# Create a yaml file, the --dry-run=client tells kubectl to output
# the definition instead of actually creating the object via the API.
kubectl run kubia --image=luksa/kubia:1.0 --dry-run=client -o yaml > kubia.yaml

# The kubectl apply command is used for creating objects as well as for making changes to
# existing objects.
kubectl apply -f kubia.yaml

## 5.2.3   Checking the newly created pod
# Retrieving the full manifest of a running pod
kubectl get po kubia -o yaml

# Using kubectl describe to see pod details
kubectl describe pod kubia

## 5.3.1   Sending requests to the application in the pod
# Getting the pod’s IP address
kubectl get pod kubia -o wide

# Connecting from a one-off client pod
kubectl run --image=tutum/curl -it --restart=Never --rm client-pod curl 10.244.2.4:8080

# Port fordward to dev/test
kubectl port-forward kubia 8080

## 5.3.2   Viewing application logs
# Viewing application logs
kubectl logs kubia

# Streaming logs using kubectl logs -f
kubectl logs kubia -f

# Displaying the timestamp of each logged line
kubectl logs kubia --timestamps=true

# Displaying recent logs
kubectl logs kubia --since=2m
kubectl logs kubia –-since-time=2020-02-01T09:50:00Z

# Displaying the last several lines of the log
kubectl logs kubia --tail=10

## 5.3.3 Copying files to and from containers

kubectl cp kubia:/etc/hosts /tmp/kubia-hosts

kubectl cp /path/to/local/file kubia:path/in/container

## 5.3.4   Executing commands in running containers

# Invoking a single command in the container
kubectl exec kubia -- ps aux
kubectl exec kubia -- curl -s localhost:8080

# Running an interactive shell in the container
kubectl exec -it kubia -- bash

## 5.3.5   Attaching to a running containerdf
kubectl attach kubia # Attach to stdout stream

# The command-line argument 8888:8080 instructs the command
# to forward local port 8888 to the pod’s port 8080.
kubectl port-forward kubia-stdin 8888:8080