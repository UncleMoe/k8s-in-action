## 5.4.1   Extending the kubia Node.js application using the Envoy proxy
# The kubia-ssl.yaml manifest is used here
kubectl port-forward kubia-ssl 8080 8443 9901

# Displaying logs of pods with multiple containers
kubectl logs kubia-ssl -c kubia
kubectl logs kubia-ssl -c envoy
kubectl logs kubia-ssl --all-containers

# Running commands in containers of multi-container pods
kubectl exec -it kubia-ssl -c envoy -- bash