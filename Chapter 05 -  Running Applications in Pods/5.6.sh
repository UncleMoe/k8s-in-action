## 5.6 Deleting pods and other objects
# Deleting a pod by name
kubectl delete po kubia
# By default, the kubectl delete command waits until the object no longer exists.
# To skip the wait, run the command with the --wait=false option.

# Deleting multiple pods with a single command
kubectl delete po kubia-init kubia-init-slow

# Deleting objects defined in manifest files
kubectl delete -f kubia-ssl.yaml

# Deleting objects from multiple manifest files
kubectl delete -f kubia.yaml,kubia-ssl.yaml
# You can also apply several files at the same time using this syntax

# Deploy all the manifest files from a file directory
kubectl apply -f path/to/directory
# Delete all the manifest files from a file directory
kubectl delete -f path/to/directory
# Use the --recursive flag to also scan subdirectories.

# Deleting all pods
kubectl delete po --all

# Deleting objects of most kinds
kubectl delete all --all

# delete events along with all object kinds included in all.
kubectl delete events,all --all
