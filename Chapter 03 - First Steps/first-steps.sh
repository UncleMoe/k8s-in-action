# First steps
# Creating a Deployment
$ kubectl create deployment kubia --image=luksa/kubia

# Listing deployments          
$ kubectl get deployments

# Listing pods
$ kubectl get pods

# Creating a Service
$ kubectl expose deployment kubia --type=LoadBalancer --port 8080

# Listing services
$ kubectl get svc

# Getting a single service  
$ kubectl get svc kubia

# Getting the service URL when using Minikube  
$ minikube service kubia --url

# Increasing the number of running application instances  
$ kubectl scale deployment kubia --replicas=3

# Seeing the results of the scale-out  
$ kubectl get deploy

# Listing pods
$ kubectl get pods

# If you use a single-node cluster, all your pods run on the same node.
# But in # a multi-node cluster, the three pods should be distributed throughout the
# cluster. To see which nodes the pods were scheduled to, you can use the -o wide
# option to display a more detailed pod list:
$ kubectl get pods -o wide

